// Bài 1.
function findMin() {
  var num = 0,
    sum = 0;
  for (var i = 1; i < 10000; i++) {
    sum += i;
    if (sum > 10000) {
      num = i;
      document.getElementById(
        "txtResult1"
      ).innerHTML = `Số nguyên dương nhỏ nhất: ${num}`;
      break;
    }
  }
}

// Bài 2
function calSum() {
  var inputXEl = document.getElementById("inputX").value * 1;
  var inputNEl = document.getElementById("inputN").value * 1;
  var sum = 0;
  for (var i = 1; i <= inputNEl; i++) {
    sum += Math.pow(inputXEl, i);
  }
  document.getElementById("txtResult2").innerHTML = `Tổng: ${sum}`;
}

// Bài 3
function calFactorial() {
  var inputN1El = document.getElementById("inputN1").value * 1;
  var factorial = 1;
  for (var i = 1; i <= inputN1El; i++) {
    factorial *= i;
  }
  document.getElementById(
    "txtResult3"
  ).innerHTML = `Giai thừa của ${inputN1El}: ${factorial}`;
}

// Bài 4
function createDiv() {
  var inputN2El = document.getElementById("inputN2").value * 1;
  var creDiv = "";
  for (var i = 1; i <= inputN2El; i++) {
    creDiv +=
      i % 2 == 0
        ? "<div class='bg-white text-body p-2'>Div chẳn</div>"
        : "<div class='bg-primary text-white p-2'>Div lẻ</div>";
  }
  document.getElementById("txtResult4").innerHTML = `${creDiv}`;
}
